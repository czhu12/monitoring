import zmq

port = "5678"

class Subscriber:
  def __init__(self):
    context = zmq.Context()
    self.socket = context.socket(zmq.NOBLOCK)
    #self.socket.bind("tcp://*:%s" % port)
    self.socket.bind('tcp://127.0.0.1:5678')

  def notify(self):
    print "sending message"
    self.socket.send('hi')
