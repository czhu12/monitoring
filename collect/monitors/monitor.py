import time

class Monitor:
  def setTimestamp(self):
    self.timestamp = time.time()

  def getTimestamp(self):
    return self.timestamp
