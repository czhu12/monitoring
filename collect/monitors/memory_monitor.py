from monitor import Monitor
import subprocess
import re

class MemoryMonitor(Monitor):
  def getTableName(self):
    return "memory_monitor"

  def getClass(self):
    return "memory"

  def getTitle(self):
    return "Memory Usage"

  def getKey(self, index=0):
    return "Memory"

  def collect(self, index=0):
    p = subprocess.Popen(['cat', '/proc/meminfo'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    memoryData = self.search(out)
    total, active, free = self.parseMemoryData(memoryData)
    capacity = active/total * 100
    print "Memory capacity: ",  capacity, "%"
    self.capacity = capacity
    self.setTimestamp()
    return self

  def getKey(self, index=0):
    return "capacity"

  def getValue(self, index=0):
    return self.capacity

  def search(self, text):
    found = re.findall("[0-9]+", text)
    return found

  def parseMemoryData(self, memoryData):
    total = float(memoryData[0])
    active = float(memoryData[5])
    free = total - active
    
    return total, active, free

if __name__ == "__main__":
  memoryMonitor = MemoryMonitor()
  memoryMonitor.collect()
  
