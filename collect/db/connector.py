import sqlite3

class Storage:
  def __init__(self):
    self.db = sqlite3.connect('./db/data/data.db')
    self.cursor = self.db.cursor()
  
  def insert(self, model):
    # Every model object has a key and a value.
    # It may also have an array of key, value pairs but
    # we'll save that for another time.

    key = model.getKey()
    value = model.getValue()
    timestamp = model.getTimestamp()
    tableName = model.getTableName()
    className = model.getClass()

    base_query = "INSERT INTO {0} " + \
        "(class, key, value, timestamp) " + \
        "VALUES ('{1}', '{2}', {3}, {4})"

    formatted_query = base_query.format(tableName, className, key, value, timestamp)
    print formatted_query
    self.cursor.execute(formatted_query)
    self.db.commit()
