import time
from db.connector import Storage
from intervals.interval_5_min import Interval5Min
from monitors.memory_monitor import MemoryMonitor
from subscribers.subscriber import Subscriber

class MonitorsSaver:
  def __init__(self, storage, interval):
    print "Created monitor"
    self.monitors = []
    self.storage = storage
    self.interval = interval
  
  def addMonitor(self, monitor):
    self.monitors.append(monitor)

  def setInterval(self, interval):
    self.interval = interval

  def getInterval(self):
    return self.interval

  def collectAll(self):
    for monitor in self.monitors:
      monitor.collect()
      self.storage.insert(monitor)

class MonitorScheduler:
  def __init__(self):
    self.subscribers = []
    
  def registerSubscriber(self, sub):
    self.subscribers.append(sub)

  def scheduleAtInterval(self, monitorsHolder):
    interval = monitorsHolder.getInterval()
    seconds = interval.getSeconds()

    while(True):
      monitorsHolder.collectAll()
      # Notify all subscribers
      map(lambda sub: sub.notify(), self.subscribers)

      time.sleep(seconds)

if __name__ == "__main__":
  memoryMonitor = MemoryMonitor()
  holder = MonitorsSaver(Storage(), Interval5Min())
  holder.addMonitor(MemoryMonitor())

  scheduler = MonitorScheduler()
  scheduler.registerSubscriber(Subscriber())
  scheduler.scheduleAtInterval(holder)
