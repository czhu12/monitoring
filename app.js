var express = require('express');
var fs = require('fs');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var io = require('socket.io')(9999);
var dbFile = './collect/db/data/data.db';

var sqlite3 = require('sqlite3').verbose();
var db;
var MINUTE_SECONDS = 60;
var HOUR_SECONDS = 60 * MINUTE_SECONDS
var DAY_SECONDS = 24 * HOUR_SECONDS;
var UPDATE_INTERVAL_SECONDS = 3;
var UPDATE_INTERVAL_MILLIS = UPDATE_INTERVAL_SECONDS * 1000;

String.prototype.format = function() {
  var formatted = this;
  for (var i = 0; i < arguments.length; i++) {
    var regexp = new RegExp('\\{'+i+'\\}', 'gi');
    formatted = formatted.replace(regexp, arguments[i]);
  }
  return formatted;
};

if (fs.existsSync(dbFile)) {
  db = new sqlite3.Database(dbFile);
}

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
  res.sendfile('public/index.html')
});

app.get('/data', function(req, res) {
  var distance = parseInt(req.query.distance, 10);
  if (isNaN(distance) || distance == undefined) {
    distance = 2;
  }

  getData(function(rows) {
    res.send(rows);
  }, distance * MINUTE_SECONDS, 500);
});

app.get('*', function (req, res) {
  res.send("Error");
});

init();

function getData(callback, timeRange, limit) {
  var currentTimestamp = Math.floor(Date.now() / 1000);
  var relevantTimestamp = currentTimestamp - timeRange;

  db.serialize(function() {
    db.all('SELECT COUNT(*) as rowCount FROM memory_monitor WHERE timestamp > ' + relevantTimestamp, function(err, result) {
      if (err) {
        return;
      }
      
      var rowCount = result[0].rowCount;
      var modulo = Math.ceil(rowCount / limit);
      getAllData(callback, relevantTimestamp, modulo);
    });
  });
}

function getAllData(callback, relevantTimestamp, modulo) {
  var query = 'SELECT * FROM memory_monitor WHERE timestamp > {0} AND id % {1} = 0'.format(relevantTimestamp, modulo);
  db.serialize(function() {
    db.all(query, function(err, rows) {
      if (err) {
        return;
      }

      callback(rows);
    });
  });
}

function sendUpdates() {
  var currentTimestamp = Math.floor(Date.now() / 1000);
  var relevantTimestamp = currentTimestamp - UPDATE_INTERVAL_SECONDS;
   
  db.serialize(function() {
    db.each('SELECT * FROM memory_monitor WHERE timestamp > ' + relevantTimestamp, function(err, rows) {
      if (err) {
        return;
      }

      sendData(rows);
    });
  });
}

function sendData(data) {
  io.sockets.emit("update", data);
}


setInterval(sendUpdates, UPDATE_INTERVAL_MILLIS);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

function init() {
  if (!Date.now) {
    Date.now = function() { return new Date().getTime(); }
  }
}

module.exports = app;
