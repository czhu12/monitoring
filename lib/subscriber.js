var zmq = require("zmq");

function Subscriber() {
  this.socket = zmq.socket('sub');
  this.socket.connect('tcp://127.0.0.1:5678');
  this.socket.on("*", function(message) {console.log("loel");});
}

Subscriber.prototype.send = function(message) {
  this.socket.send(message);
}

Subscriber.prototype.listen = function(callback) {
  this.socket.on("message", callback);
}

module.exports = Subscriber
